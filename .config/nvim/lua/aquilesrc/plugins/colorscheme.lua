-- Moonfly
-- vim.g.moonflyTransparent = true
-- vim.g.moonflyItalics = false
-- vim.g.moonflyNormalFloat = false
-- vim.g.moonflyCursorColor = true
-- vim.g.moonflyTerminalColors = true
-- vim.g.moonflyUndercurls = true
-- vim.g.moonflyUnderlineMatchParen = true
-- vim.g.moonflyWinSeparator = 2
-- require("moonfly").custom_colors({
--     white = "#dddddd",
--     blue = "#74b2ff",
--     sky = "#a1c0eb",
--     yellow = "#e3c78a",
--     cranberry = "#95c0d5",
--     grey11 = "#252525",
--     grey7 = "#1a1a1a"
-- })

-- Mellow
-- vim.g.mellow_italic_comments = true
-- vim.g.mellow_italic_keywords = false
-- vim.g.mellow_italic_booleans = false
-- vim.g.mellow_italic_functions = false
-- vim.g.mellow_bold_keywords = false
-- vim.g.mellow_bold_booleans = false
-- vim.g.mellow_bold_functions = true
-- vim.g.mellow_transparent = true
-- vim.g.mellow_highlight_overrides = {
--     NormalFloat = { bg = "#242424" }
-- }

-- Gruvbox Material
-- vim.g.gruvbox_material_transparent_background = 1
-- vim.g.gruvbox_material_background = 'hard'
-- vim.g.gruvbox_material_foreground = 'mix'
-- vim.g.gruvbox_material_enable_bold = 1
-- vim.g.gruvbox_material_enable_italic = 0
-- vim.g.gruvbox_material_visual = 'blue background'
-- vim.g.gruvbox_material_menu_selection_background = 'blue'
-- vim.g.gruvbox_material_ui_contrast = 'high'
-- vim.g.gruvbox_material_float_style = 'dim'

-- Fullerene
-- require("fullerene").setup({
--     integrations = {
--         telescope = false,
--         lazy = true,
--         cmp = true,
--         lsp = true,
--         mason = true,
--         treesitter = true,
--     },
--     styles = {
--         comments = { italic = true },
--         functions = { bold = false },
--     },
--     highlight_overrides = {
--         Normal = { bg = "none", fg = "#eeeeee" },
--         NormalNC = { bg = "none" },
--         StatusLine = { bg = "none" },
--         -- StatusLine = { bg = "#252525" },
--         NormalFloat = { bg = "#222222" },
--         LineNr = { fg = "#5c5c5c" },
--         PmenuSel = { bg = "#454545" },
--         CursorLine = { bg = "#282828" },
--         ColorColumn = { bg = "#282828" },
--         ModeMsg = { fg = "#c9c7cd" },
--         SignColumn = { bg = "none" },
--         FoldColumn = { bg = "none" },
--     }
-- })

-- Tundra
require('nvim-tundra').setup({
    transparent_background = true,
    dim_inactive_windows = {
        enabled = false,
        color = nil,
    },
    sidebars = {
        enabled = true,
        color = nil,
    },
    editor = {
        search = {},
        substitute = {},
    },
    syntax = {
        booleans = { bold = true },
        comments = { bold = true },
        conditionals = {},
        constants = { bold = true },
        fields = {},
        functions = {},
        keywords = {},
        loops = {},
        numbers = { bold = true },
        operators = { bold = true },
        punctuation = {},
        strings = { bold = true },
        types = { bold = true },
    },
    diagnostics = {
        errors = {},
        warnings = {},
        information = {},
        hints = {},
    },
    plugins = {
        lsp = true,
        treesitter = true,
        telescope = false,
        cmp = true,
    },
    overwrite = {
        colors = {},
        highlights = {
            Normal = { fg = "#eeeeee" },
            NormalFloat = { bg = "#212121" },
            StatusLine = { bg = "#252525" },
            -- StatusLine = { bg = "none" },
            CursorLine = { bg = "#303030" },
            ColorColumn = { bg = "#363636" },
            -- Keyword = { fg = "#BAE6FD" },
            -- Type = { fg = "#BAE6FD" },
        },
    },
})
vim.g.tundra_biome = 'jungle' -- 'arctic' | 'jungle'

vim.cmd.colorscheme "tundra"
