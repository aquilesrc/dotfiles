-----------------
-- Keybindings --
-----------------
vim.g.mapleader = " "
local map = vim.keymap.set

map("n", "<Esc>", "<cmd>nohlsearch<CR>")

map("n", "<C-d>", "<C-d>zz")
map("n", "<C-u>", "<C-u>zz")
map("n", "<leader>c", ":")

map("n", "<leader>e", vim.cmd.Ex)

map("n", "<leader>f", vim.cmd.Format)

map("n", "<leader>w", vim.cmd.write)
map("n", "<leader>q", vim.cmd.quit)
map("n", "<leader>W", vim.cmd.wall)
map("n", "<leader>Q", vim.cmd.quitall)

map("n", "<F2>", "<cmd>set background=light<CR>")
map("n", "<F3>", "<cmd>set background=dark<CR>")

-- Splits
map("n", "<leader>ss", "<C-w>v<C-w>l")
map("n", "<leader>sh", "<C-w>s<C-w>j")

map("n", "<A-h>", "<C-w>h")
map("n", "<A-j>", "<C-w>j")
map("n", "<A-k>", "<C-w>k")
map("n", "<A-l>", "<C-w>l")

-- Terminal
map("n", "<leader>t", "<C-w>s<C-w>j:term<CR>a")
