local M = {}

M.colors = {
    fg = "#eeeeee",
    fg_1 = "#f4f4ff",
    fg_2 = "#f5f5f5",
    bg = "#0f0f0f",
    bg_m1 = "#101010",
    bg_1 = "#242424",
    bg_2 = "#453d41",
    bg_3 = "#484848",
    bg_4 = "#676767",
    white = "#ffffff",
    black = "#000000",
    red = "#dc6068",
    green = "#8cc85f",
    yellow = "#ffcb6b",
    cyan = "#79dac8",
    blue = "#a1c0eb",
    magenta = "#c6abf7",
}

M.setup = function()
    local colors = M.colors
    local config = require("gruber-darker").config

    if config.transparent then
        colors.bg = "NONE"
    end

    local groups = {

        Normal = { bg = colors.bg, fg = colors.fg },
        NormalFloat = { bg = colors.bg_1 },
        NonText = { link = "Normal" },

        FloatBorder = { fg = colors.fg },

        Cursor = { fg = colors.yellow },
        CursorLine = { bg = colors.bg_1 },

        ColorColumn = { bg = colors.bg_1 },
        SignColumn = { link = "Normal" },

        LineNr = { fg = colors.yellow },
        LineNrAbove = { fg = colors.bg_4 },
        LineNrBelow = { fg = colors.bg_4 },

        Comment = { fg = colors.bg_4 },
        String = { fg = colors.green },
        Number = { fg = colors.white },

        Boolean = { fg = colors.yellow },
        Float = { fg = colors.white },
        Character = { fg = colors.green },

        Identifier = { fg = colors.fg_1 },
        Operator = { link = "Normal" },
        Title = { fg = colors.blue },

        Constant = { fg = colors.cyan },
        Keyword = { fg = colors.yellow, bold = config.bold },
        -- Statement = {},
        -- Conditional = {},
        -- Repeat = {},
        -- Exception = {},

        Function = { fg = colors.blue },

        PreProc = { fg = colors.cyan },
        Include = { link = "PreProc" },
        Define = { link = "PreProc" },
        Macro = { link = "PreProc" },
        Precondit = { link = "PreProc" },

        Type = { fg = colors.cyan },
        Typedef = { fg = colors.yellow },
        -- Structure = { },
        -- StorageClass = { },

        Special = { link = "Normal" },
        SpecialChar = { link = "String" },
        SpecialComment = { fg = colors.green },
        Todo = { fg = colors.magenta },
        MatchParen = { bg = colors.bg_4 },
        -- Underlined = {},

        Error = { fg = colors.red },
        ErrorMsg = { fg = colors.red },
        WarningMsg = { fg = colors.red },

        netrwBak = { fg = colors.cyan },
        Directory = { fg = colors.blue },
        netrwDir = { fg = colors.blue },
        netrwExe = { fg = colors.green },
        netrwLink = { fg = colors.yellow },

        DiagnosticError = { fg = colors.red },
        DiagnosticWarn = { fg = colors.yellow },
        DiagnosticInfo = { fg = colors.green },

        Pmenu = { bg = colors.bg_1 },
        PmenuSel = { bg = colors.bg_m1 },
        PmenuThumb = { link = "Pmenu" },

        Search = { bg = colors.fg_2, fg = colors.black },
        IncSearch = { bg = colors.fg_1, fg = colors.blue },

        Visual = { bg = colors.bg_3 },

        Question = { fg = colors.fg },
        MoreMsg = { fg = colors.fg },

        WinSeperator = { link = "Normal" },
        EndOfBuffer = { bg = colors.bg, fg = colors.bg_1 },

        -- TermCursor = { },
        -- TermCursorNC = {},

        DiffText = { link = "Normal" },
        DiffDelete = { fg = colors.red },
        DiffAdd = { fg = colors.green },
        DiffChange = { fg = colors.yellow },

        SpellBad = { fg = colors.red, underline = config.underline },
        SpellCap = { fg = colors.yellow, underline = config.underline },

        StatusLine = { bg = "NONE", fg = colors.white },
        StatusLineNC = { bg = colors.bg_1, fg = colors.cyan },

        VertSplit = { fg = colors.bg_2 },

        QuickFixLine = { link = "CursorLine" },

        -- fugitive.nvim
        diffAdded = { link = "DiffAdd" },
        diffRemoved = { link = "DiffDelete" },

        -- html
        htmlLink = { fg = colors.blue, underline = config.underline },
        htmlTag = { fg = colors.cyan },
        htmlTagName = { fg = colors.fg },

        -- markdown
        markdownH1 = { fg = colors.blue },
        markdownH2 = { fg = colors.cyan },
        markdownH3 = { fg = colors.magenta },
        markdownH4 = { fg = colors.yellow },
        markdownH5 = { fg = colors.red },
        markdownBlockquote = { fg = colors.bg_4 },
        markdownUrl = { fg = colors.blue, underline = config.underline },

        -- telescope.nvim
        -- TelescopeNormal = { },
        -- TelescopeSelection = { },
        -- TelescopeSelectionCaret = { },
        -- TelescopeMultiSelection = { },
        -- TelescopeMatching = { },
        -- TelescopePrompt = { },
        -- TelescopePromptPrefix = { },
        -- TelescopeBorder = { },
        -- TelescopePromptBorder = { },
        -- TelescopeResultsBorder = { },
        -- TelescopePreviewBorder = { },
    }
    return groups
end

return M
