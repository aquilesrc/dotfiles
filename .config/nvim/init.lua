require("aquilesrc")

-- Basic config
require("aquilesrc.options")
require("aquilesrc.keybindings")

-- Plugins
require("aquilesrc.lazy")
require("aquilesrc.plugins.treesitter")
require("aquilesrc.plugins.lsp")
require("aquilesrc.plugins.cmp")
require("aquilesrc.plugins.telescope")
require("aquilesrc.plugins.harpoon")
require("aquilesrc.plugins.luasnip")
require("aquilesrc.plugins.autoclose")
require("aquilesrc.plugins.colorscheme")
