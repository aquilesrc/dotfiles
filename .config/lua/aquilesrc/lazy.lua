local lazypath = vim.fn.stdpath "data" .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system {
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    }
end
vim.opt.rtp:prepend(lazypath)

-- Plugins Instalattion --
require("lazy").setup({
    -- Color schemes
    -- { "bluz71/vim-moonfly-colors" },
    -- { "mellow-theme/mellow.nvim" },
    -- { "sainnhe/gruvbox-material" },
    { "sam4llis/nvim-tundra" },
    -- { "navarasu/onedark.nvim" },
    -- { "steguiosaur/fullerene.nvim" },

    -- { dir = "/home/aquilesrc/.config/nvim/themes/patana", lazy = true },

    -- Treesitter
    "nvim-treesitter/nvim-treesitter",

    -- Fuzzy finder
    {
        "nvim-telescope/telescope.nvim",
        dependencies = { "nvim-lua/plenary.nvim" }
    },

    { "ThePrimeagen/harpoon",    branch = "harpoon2", },

    -- LSP Related Plugins
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            -- Mason
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",

            -- Additional lua configuration
            "folke/neodev.nvim"
        },

    },

    {
        -- Autocompletion
        "hrsh7th/nvim-cmp",
        dependencies = {
            -- LSP Completion
            "hrsh7th/cmp-nvim-lsp",

            -- Friendly snippets
            --"rafamadriz/friendly-snippets",
        }
    },

    { "m4xshen/autoclose.nvim" },

    { "L3MON4D3/LuaSnip",        after = "nvim-cmp", },
    { "saadparwaiz1/cmp_luasnip" }
})
