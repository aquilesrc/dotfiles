pcall(require('telescope').load_extension, 'fzf')
require('telescope').setup({
    defaults = {
        layout_strategy = 'flex',
        layout_config = {
            bottom_pane = {
                height = 20,
                preview_cutoff = 200,
                prompt_position = "bottom"
            },
            center = {
                height = 0.6,
                preview_cutoff = 40,
                prompt_position = "top",
                width = 0.5
            },
            cursor = {
                height = 0.9,
                preview_cutoff = 40,
                width = 0.8
            },
            horizontal = {
                height = 0.9,
                preview_cutoff = 80,
                prompt_position = "bottom",
                width = 0.9
            },
            vertical = {
                height = 0.9,
                preview_cutoff = 20,
                prompt_position = "bottom",
                width = 0.9
            }
        },

        file_ignore_patterns = {
            "go/pkg", "Vídeos", "Imagens", "Jogos", "Desktop", "Livros",
            "Músicas", "Livros", "Pessoal", "Comics", "Videos", ".doc",
            ".docx", ".odt", ".out", ".mp3"
        }
    },

    pickers = {
        diagnostics = {
            --theme = "ivy",
            layout_strategy = "vertical",
        }
    },
})

-- vim.keymap.set('n', '<leader>/', function()
--     require('telescope.builtin').current_buffer_fuzzy_find(require('telescope.themes').get_dropdown {
--         winblend = 10,
--         previewer = true,
--     })
-- end, { desc = '[/] Fuzzily search in current buffer' })

-- vim.keymap.set('n', '<leader>t', vim.cmd.Telescope)
vim.keymap.set('n', '<leader><space>', require('telescope.builtin').buffers)
vim.keymap.set('n', '<leader>f', require('telescope.builtin').find_files, { desc = 'Search Files' })
vim.keymap.set('n', '<leader>r', require('telescope.builtin').oldfiles)
vim.keymap.set('n', '<leader>/', require('telescope.builtin').current_buffer_fuzzy_find)
vim.keymap.set('n', '<leader>m', require('telescope.builtin').man_pages, { desc = 'View Man Pages' })
vim.keymap.set('n', '<leader>d', require('telescope.builtin').diagnostics, { desc = 'Search Diagnostics' })
--vim.keymap.set('n', '<leader>cs', require('telescope.builtin').colorscheme, { desc = 'Change Colorscheme' })

vim.keymap.set('n', '<leader>g', require('telescope.builtin').live_grep, { desc = 'Live Grep' })

--vim.keymap.set('n', '<leader>sw', require('telescope.builtin').grep_string, { desc = '[S]earch current [W]ord' })
--vim.keymap.set('n', '<leader>sh', require('telescope.builtin').help_tags, { desc = 'Search Help' })
--vim.keymap.set('n', '<leader>gf', require('telescope.builtin').git_files, { desc = 'Search [G]it [F]iles' })
-- vim.keymap.set('n', '<leader>sg', require('telescope.builtin').live_grep, { desc = '[S]earch by [G]rep' })
--vim.keymap.set('n', '<leader>sr', require('telescope.builtin').resume, { desc = '[S]earch [R]esume' })
