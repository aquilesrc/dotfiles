-----------------
-- Keybindings --
-----------------
vim.g.mapleader = " "
local map = vim.keymap.set

map("n", "<Esc>", "<cmd>nohlsearch<CR>")

-- Re-center on scroll
map("n", "<C-d>", "<C-d>zz")
map("n", "<C-u>", "<C-u>zz")

map("n", "<leader>c", ":") -- Enter command mode
map("n", "<leader>e", vim.cmd.Ex) -- Open netrw

-- Buffer Bindings
map("n", "<leader>bw", vim.cmd.write) -- Write buffer changes
map("n", "<leader>bq", vim.cmd.quit) -- Close buffer
map("n", "<leader>bf", vim.cmd.Format) -- Format buffer 
map("n", "<leader>bW", vim.cmd.wall) -- Write all buffers changes
map("n", "<leader>bQ", vim.cmd.quitall) -- Close all buffers

-- Splits
map("n", "<leader>ss", "<C-w>v<C-w>l") -- Vertical split
map("n", "<leader>sS", "<C-w>s<C-w>j") -- Horizontal split
map("n", "<leader>sh", "<C-w>h") -- Focus left split
map("n", "<leader>sj", "<C-w>j") -- Focus right split
map("n", "<leader>sk", "<C-w>k") -- Focus split above
map("n", "<leader>sl", "<C-w>l") -- Focus split below
map("n", "<leader>t", "<C-w>s<C-w>j:term<CR>a") -- Terminal split
