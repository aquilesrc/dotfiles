-------------
-- Options --
-------------
vim.opt.shell = "fish"

vim.opt.number = true
vim.opt.numberwidth = 2
vim.opt.relativenumber = true
vim.wo.number = true
vim.opt.nu = true
--vim.opt.guicursor = "n-v-c-sm-i-ci-ve:block"

vim.opt.linebreak = true
vim.opt.title = true
vim.opt.showmode = true

vim.opt.colorcolumn = "80"
vim.opt.cursorline = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.smartindent = true

vim.opt.hlsearch = true
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.breakindent = true

vim.opt.undofile = true

vim.opt.completeopt = "menuone,noselect"
vim.opt.mouse = ""
vim.opt.signcolumn = "yes"
vim.opt.clipboard = "unnamedplus"

vim.opt.scrolloff = 8
vim.g.netrw_bufsettings = 'noma nomod nu rnu nobl nowrap ro'
